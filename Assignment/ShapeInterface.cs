﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    interface ShapeInterface
    {
        void set(Color colour, params int[] posAndSides); //setting a variable for colour and an array for x, y positions and however many sides of the shape
        void draw(Graphics g); //a method to draw the shape and add fill if needed
    }
}
