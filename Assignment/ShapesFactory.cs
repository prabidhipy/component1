﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// a factory so that there are not a lot of dependencies of the classes
    /// also to hide object creations of the classes
    /// </summary>
    class ShapesFactory
    {
        /// <summary>
        /// method to get the shape type and initialise an object for each one
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Shapes GetShape(String type)
        {
            type= type.ToUpper().Trim();

            if(type.Equals("RECTANGLE"))
            {
                return new Rectangle();
            }
            else if(type.Equals("CIRCLE"))
            {
                return new Ellipse();
            }
            else if(type.Equals("TRIANGLE"))
            {
                return new Triangle();
            }
            else if (type.Equals("LINE"))
            {
                return new Line();
            }
            else
            {
                System.ArgumentException exc= new System.ArgumentException(type + " shape type doesnot exist");
                throw exc; //throwing an exception if the shape type doesnot exist
            }
        }
    }
}
