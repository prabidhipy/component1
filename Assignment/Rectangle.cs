﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// to draw a rectangle of different shapes and sizes, to add fill and change colours, extends shapes
    /// </summary>
    class Rectangle : Shapes
    {
        int width, height; //height and width of the rectangle
        bool colourFill;

        public Rectangle() : base()
        {
            width = 0;
            height = 0;
        }

        public Rectangle(Color colour, int xPos, int yPos, int width, int height) : base(colour, xPos, yPos)
        {
            this.width = width; //setting the width and height since it has not been done in the base class
            this.height = height;
        }

        public override void fill(bool colourFill)
        {
            this.colourFill = colourFill;
        }

        /// <summary>
        /// setting colour, x, y position and height and width of the rectangle
        /// </summary>
        /// <param name="colour"></param> colour of the rectangle fill or outline
        /// <param name="posAndSides"></param> x, y , width and height of the rectangle
        public override void set(Color colour, params int[] posAndSides)
        {
            base.set(colour, posAndSides[0], posAndSides[1]); //array of posAndSides[0] and [1] have been set as the x and y position in the base class
            this.width = posAndSides[2];//the next array list, posAndSides[2] is hence, the width
            this.height = posAndSides[3];//the next array list, posAndSides[3] will now be the height
        }
        /// <summary>
        /// drawing a rectangle with or without the fill
        /// </summary>
        /// <param name="g"></param>
        /// <param name="colourFill"></param> colour to fill the rectangle
        public override void draw(Graphics g)
        {
            Pen myPen = new Pen(colour); //pen to draw
            SolidBrush myBrush = new SolidBrush(colour); //brush to fill

            g.DrawRectangle(myPen, xPos, yPos, width, height);

            //if (colourFill)
            //{
            //    g.FillRectangle(myBrush, xPos, yPos, width, height);
            //}
        }
    }
}
