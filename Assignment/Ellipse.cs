﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment
{
    /// <summary>
    /// class to create circles of different sizes, move to different places and add fill, extends shape
    /// </summary>
     class Ellipse : Shapes
     {
        int radius;
        bool colourFill;

        //default constructor for the class
        public Ellipse() : base()
        {

        }

        //parameterised constructor 
        public Ellipse(Color colour, int xPos, int yPos, int radius) : base(colour, xPos, yPos)
        {
            this.radius = radius; //setting a radius as it has not been set in the base class
        }

        /// <summary>
        /// sets the radius, x,y position and colour for the circle
        /// </summary>
        /// <param name="colour"></param> outline or fill of the circle
        /// <param name="posAndSides"></param> x,y and radius 
        public override void set(Color colour, params int[] posAndSides)
        {
            base.set(colour, posAndSides[0], posAndSides[1]); //array of posAndSides[0] and [1] have been set as the x and y position in the base class
            this.radius = posAndSides[2];//the next array list, posAndSides[2] is hence, the radius
        }

        public override void fill(bool colourFill)
        {
            this.colourFill = colourFill;
        }
        /// <summary>
        /// draws or fills the circle with colour
        /// </summary>
        /// <param name="g"></param>
        /// <param name="colourFill"></param> colour to fill the shape  
        public override void draw(Graphics g)
        {
            Pen myPen = new Pen(colour); //for the outline of the circle
            SolidBrush myBrush = new SolidBrush(colour); //to fill the circle

            g.DrawEllipse(myPen, xPos, yPos, 2 * radius, 2 * radius);

            if (colourFill) //if colourFill is true 
            {
                g.FillEllipse(myBrush, xPos, yPos, 2 * radius, 2 * radius);  
            }
        }
    }
}
