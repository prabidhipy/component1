﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Collections;

namespace Assignment
{
    /// <summary>
    /// a form to create shapes and run various commands
    /// </summary>
    public partial class Form1 : Form
    {
        SolidBrush solidBrush = new SolidBrush(Color.Red); //to fill any shape
        Color shapeColour = Color.Black; //defining the default shape colour as black
        public int xPos, yPos; //x position and y position
        ArrayList content; //file content in an arraylist as the content length is undefined
        bool colourFill = false; //fill of the shape set as false by default
        public string programCode;
        public string variable1, variable2;

        ArrayList shapes = new ArrayList(); //initialising an object for arraylist
        public Form1()
        {
            InitializeComponent();

            ShapesFactory factory = new ShapesFactory(); //object of ShapesFactory

            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
        }

        /// <summary>
        /// This method will load a new file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            multilineCode.Text = " "; //remove any text from the text box before loading a file
            content = new LoadAndSaveFile().OpenFile();

            foreach (string line in content)
            {
                multilineCode.Text += line + "\r\n"; //display the text file content in different lines
            }
        }

        /// <summary>
        /// to save a code block written in the text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new LoadAndSaveFile().SaveFile(multilineCode.Text); //calling Savefile method from another class
        }

        /// <summary>
        /// to exit the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// checks the conditions and executes the appropriate lines to run the commands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();

            using (var g = Graphics.FromImage(pictureBox1.Image))
            {
                //String  = lineCommand.Text.ToUpper(); //converting textBox6 text into uppercase

                //if (lineCommand == "RUN")
                //{

                //}
                //else if (lineCommand == "RESET")
                //{
                //    xPos = yPos = 0; //setting x and y position of cursor to default
                //    multilineCode.ResetText();  //resetting the text 
                //}
                //else if (lineCommand == "CLEAR")
                //{
                //    multilineCode.Clear(); //clearing the text
                //}
                //else
                //{
                //    Console.WriteLine("Invalid command. Please try Run, Reset or Clear");
                //}

                if (multilineCode.Text.Contains('\n') || multilineCode.Text.Contains(',') || multilineCode.Text.Contains(' '))
                {
                    string[] splitLine = multilineCode.Text.ToUpper().Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                    for (int i = 0; i < splitLine.Length; i++)
                    {
                        string[] splitCmd = splitLine[i].Split(' ', ','); //splitting the text

                        for (int j = 0; j < 1; j++)
                        {
                            //for fill on/ off command
                            if (splitCmd.Length == 2 && splitCmd[0].Equals("FILL"))
                            {
                                if (splitCmd[1].Equals("ON"))
                                {
                                    colourFill = true;
                                }
                                else if (splitCmd[1].Equals("OFF"))
                                {
                                    colourFill = false;
                                }
                            }
                            //for pen <colour> command
                            else if (splitCmd.Length == 2 && splitCmd[0].Equals("PEN"))
                            {
                                ///setting the shapeColour to different colours defined here
                                if (splitCmd[1].Equals("BLUE"))
                                {
                                    shapeColour = Color.Blue;
                                }
                                else if (splitCmd[1].Equals("RED"))
                                {
                                    shapeColour = Color.Red;
                                }
                                else if (splitCmd[1].Equals("PURPLE"))
                                {
                                    shapeColour = Color.Purple;
                                }
                                else if (splitCmd[1].Equals("GREEN"))
                                {
                                    shapeColour = Color.Green;
                                }
                                else if (splitCmd[1].Equals("BLACK"))
                                {
                                    shapeColour = Color.Black;
                                }
                            }

                            //if the split text has 2 values
                            if (splitCmd.Length == 2)
                            {
                                if (splitCmd[0].Equals("CIRCLE")) //draw a circle
                                {
                                    int fParameter = int.Parse(splitCmd[1]); //first parameter as the second split text

                                    Shapes s = ShapesFactory.GetShape("CIRCLE");
                                    s.set(shapeColour, xPos, yPos, fParameter); //calling set method from base class
                                    s.fill(colourFill);

                                    shapes.Add(s);
                                }
                            }
                            //if split text is 3
                            else if (splitCmd.Length == 3)
                            {
                                if (splitCmd[1] == "=" && splitLine.Length == 2)
                                {
                                    variable1 = splitCmd[0];
                                   
                                    //string myString = variable1.ToString();
                                    splitCmd[2] = variable1; //stringCmd[0] = a and splitCmd[2] = 20, so, a = 20               
                                }
                                else {
                                    int fParameter = int.Parse(splitCmd[1]);
                                    int sParameter = int.Parse(splitCmd[2]);

                                    //if the command is moveto x,y
                                    if (splitCmd[0].Equals("MOVETO"))
                                    {
                                        xPos = fParameter;
                                        yPos = sParameter;
                                    }
                                    //if the command is rectangle x,y 
                                    else if (splitCmd[0].Equals("RECTANGLE"))
                                    {
                                        Shapes s = ShapesFactory.GetShape("RECTANGLE");
                                        s.set(shapeColour, xPos, yPos, fParameter, sParameter);
                                        s.fill(colourFill);

                                        shapes.Add(s);
                                    }
                                    //if the command is drawto x,y
                                    else if (splitCmd[0].Equals("DRAWTO"))
                                    {
                                        Shapes s = ShapesFactory.GetShape("LINE");
                                        s.set(shapeColour, xPos, yPos, fParameter, sParameter);

                                        shapes.Add(s);
                                    }
                                    else if (splitCmd[0].Equals("TRIANGLE")) //draw a triangle
                                    {
                                        Shapes s = ShapesFactory.GetShape("TRIANGLE"); //calling method from shapes factory class
                                        s.set(shapeColour, xPos, yPos, fParameter, sParameter); //calling set method from base class
                                        s.fill(colourFill);

                                        shapes.Add(s);
                                    }
                                }
                            }
                        }
                    }

                    pictureBox1.Refresh(); //refresh the picturebox
                }
            }
        }

        public String[] multiline(String text)
        {
            //accepts a text and returns array of line strings.
            String TextParse = text.ToLower().Trim();
            String[] split = TextParse.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            return split;
        }

        public void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillEllipse(solidBrush, xPos, yPos, 6, 6); //a cursor or pointer in the picturebox to show where x and y positions are

            for (int i = 0; i < shapes.Count; i++)
            {
                Shapes s;
                s = (Shapes)shapes[i];

                if (s != null)
                {
                    s.draw(g); //calling the draw method from the draw class
                }
            }
        }

    }
}
