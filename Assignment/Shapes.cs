﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment
{
    /// <summary>
    /// the abstract base class for all shapes that implements interface shapeinterface
    /// </summary>
    abstract class Shapes :ShapeInterface
    {
        protected Color colour; //colour for the shape
        protected int xPos, yPos; //position of the shape
        public Shapes()
        {

        }

        /// <summary>
        /// colour, x and y position that is needed for all shapes
        /// </summary>
        /// <param name="colour"></param>colour of the shape
        /// <param name="xPos"></param> x position 
        /// <param name="yPos"></param> y position
        public Shapes(Color colour, int xPos, int yPos)
        {
            this.colour = colour;  
            this.xPos = xPos;
            this.yPos = yPos;
        }

        public abstract void draw(Graphics g); //all the child classes will have to implement this to draw the shape
        public abstract void fill(bool colourFill);

        public virtual void set(Color colour, params int[] posAndSides) //to set the colour, x,y and sides of the child classes
        {
            this.colour = colour; //colour of any given shape
            this.xPos = posAndSides[0]; //the x position of any given shape
            this.yPos = posAndSides[1]; // the y position of any given shape
        }
    }
}
