﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment
{/// <summary>
/// a class to load and save files to and from the text box
/// </summary>
    class LoadAndSaveFile
    {
        ArrayList content; //arraylist content because content of the file is undefined
        string filePath; //path of our selected file

        /// <summary>
        /// To open a file into the command box 
        /// </summary>
        /// <returns></returns>
        public ArrayList OpenFile()
        {
            using (OpenFileDialog open = new OpenFileDialog()) //creating an instance of OpenFileDialog
            {
                content = new ArrayList();

                open.InitialDirectory = "C:\\Users\\prabi\\source\\repos\\Assignment\\component1\\Assignment"; //initialising a path to where the test codes are
                open.Filter = "txt files(*.txt)|*.txt"; //accepts text files

                if (open.ShowDialog() == DialogResult.OK)
                {
                    filePath = open.FileName;
                }
            }
            try //using try to catch any file not found exceptions that might cause problems to the program
            {
                using (StreamReader s = File.OpenText(filePath))
                {
                    do
                    {
                        string line = s.ReadLine(); //reads each line of file
                        if (line == null) break; //ends there if the there is no next line
                        content.Add(line); //adds a line to the arraylist content
                    } while (true); 

                }
            }
            //if the file is not found
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error. No file found.");
            }
            return content; 
        }
        /// <summary>
        /// To save our codes into a new file
        /// </summary>
        /// <param name="code"></param>
        public void SaveFile(String code)
        {
            using (SaveFileDialog save = new SaveFileDialog())
            {
                save.InitialDirectory = "C:\\Users\\prabi\\source\\repos\\Assignment\\component1\\Assignment"; //initialising a path to where we want to save our files
                save.Filter = "txt files (*.txt)|*.txt"; //we can only save text files
                save.ShowDialog();

                if (save.FileName != "")
                {
                    filePath = Path.GetFullPath(save.FileName); //get path name if there is one
                    save.Dispose();
                }
            }
            //try catch so that there is no errors 
            try
            {
                using (StreamWriter s = File.CreateText(filePath))
                {
                    s.WriteLine(code);
                }
            }
            catch (IOException ex)
            {

            }
        }
    }
}
