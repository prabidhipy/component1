﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// class to create triangles, add fills and change colours 
    /// extends Shapes
    /// </summary>
    class Triangle:Shapes
    {
        int width, height; //sides of the triangle
        bool colourFill;

        public Triangle() : base()
        {

        }
        public Triangle(Color colour, int xPos, int yPos, int width, int height): base(colour, xPos, yPos)
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// setting colour, sides, x and y position of triangle
        /// </summary>
        /// <param name="colour"></param> colour of triangle
        /// <param name="posAndSides"></param>x,y position and sides of triangle
        public override void set(Color colour, params int[] posAndSides)
        {
            base.set(colour, posAndSides[0], posAndSides[1]); //array 0 and 1 are x and y positions respectively
            this.width = posAndSides[2]; //side 1 of the triangle
            this.height = posAndSides[3]; //side 2 of the triangle
        }

        public override void fill(bool colourFill)
        {
            this.colourFill = colourFill;
        }

        /// <summary>
        /// calling draw method from Shapes to draw and/or fill the triangle
        /// </summary>
        /// <param name="g"></param>
        /// <param name="colourFill"></param> to add fill to the triangle if needed
        public override void draw(Graphics g)
        {
            Pen myPen = new Pen(colour); //pen to draw
            SolidBrush myBrush = new SolidBrush(colour); //brush to fill

            Point[] sides = new Point[3];

            sides[0].X = xPos;
            sides[0].Y = yPos;

            sides[1].X = xPos + width/2;
            sides[1].Y = yPos;

            sides[2].X = xPos;
            sides[2].Y = yPos + height/2;

            g.DrawPolygon(myPen, sides);

            if (colourFill)
            {
                g.FillPolygon(myBrush, sides);
            }
        }
    }
}
