﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class Compile
    {
        public String[] TextParser(String text)
        {
            //accepts a text and returns array of line strings.
            String TextParse = text.ToLower().Trim();
            String[] split = TextParse.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            return split;
        }
    }
}
