﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    /// <summary>
    /// to draw a line using the drawto command, extends shapes
    /// </summary>
    class Line:Shapes
    {
        int x1, y1;
        bool colourFill;

        public Line() : base()
        {
            x1 = 0;
            y1 = 0;
        }

        public Line(Color colour, int xPos, int yPos, int x1, int y1) : base(colour, xPos, yPos)
        {
            this.x1 = x1;
            this.y1 = y1;
        }
        /// <summary>
        /// sets the colour, and x, y positions for the line
        /// </summary>
        /// <param name="colour"></param> colour of the line
        /// <param name="posAndSides"></param> x and y positions
        public override void set(Color colour, params int[] posAndSides)
        {
            base.set(colour, posAndSides[0], posAndSides[1]); 
            this.x1 = posAndSides[2];
            this.y1 = posAndSides[3];
        }

        public override void fill(bool colourFill)
        {
            this.colourFill = colourFill;
        }

        /// <summary>
        /// draws a line
        /// </summary>
        /// <param name="g"></param>
        /// <param name="colourFill"></param> 
        public override void draw(Graphics g)
        {
            Pen myPen = new Pen(colour);

            g.DrawLine(myPen, xPos, yPos, x1, y1);
        }
    }
}
